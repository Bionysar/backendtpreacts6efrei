var express = require("express");
var router = express.Router();
var mysql = require("mysql");
var connection = mysql.createConnection({
  host: "217.182.237.222",
  user: "laurent",
  password: "admin",
  database: "TopMaterielAchat",
  port: 3306
});
var result = connection.state;

connection.connect(err => {
  if (!err) {
    console.log("DB connection succeded.");
    //result = connection.state;
  } else
    console.log(
      "DB connection failed \n Error : " + JSON.stringify(err, undefined, 2)
    );
});

router.get("/", function(req, res, next) {
  connection.query(
    "SELECT productID, titre, imagePath, price, imageAlt, rc.name AS category, rb.name AS brand, quantity  FROM Products p INNER JOIN RefCategorie rc ON p.idCategorie = rc.idCategorie INNER JOIN RefBrand rb ON p.idBrand = rb.idBrand",
    function(err, rows, fields) {
      if (err) {
        return console.error(err.message);
      }
      result = rows;
      if (result.length > 0) {
        var array = [];
        for (var i = 0; i < result.length; i++) {
          array.push({
            id: result[i].productID,
            picture: result[i].imagePath,
            price: result[i].price,
            name: result[i].titre,
            alt: result[i].imageAlt,
            category: result[i].category,
            brand: result[i].brand,
            quantity: result[i].quantity
          });
        }
        res.end(JSON.stringify(array));
      }
    }
  );
});

module.exports = router;
