var express = require("express");
var router = express.Router();
var mysql = require("mysql");
var cookieParser = require("cookie-parser");
var cors = require("cors");

var connection = mysql.createConnection({
  host: "217.182.237.222",
  user: "laurent",
  password: "admin",
  database: "TopMaterielAchat",
  port: 3306
});
var result = connection.state;
router.use(cookieParser());

connection.connect(err => {
  if (!err) {
    console.log("DB connection succeded.");
    //result = connection.state;
  } else
    console.log(
      "DB connection failed \n Error : " + JSON.stringify(err, undefined, 2)
    );
});

router.post("/", function(req, res, next) {
  connection.query(
    'SELECT title, description FROM `ProductCharateristics` WHERE ProductID ="' +
      req.body.Data.id +
      '"',
    function(err, rows, fields) {
      if (err) {
        return console.error(err.message);
      }
      result = rows;
      if (result.length > 0) {
        var array = [];
        for (var i = 0; i < result.length; i++) {
          array.push({
            title: result[i].title,
            description: result[i].description
          });
        }
        res.end(JSON.stringify(array));
      }
    }
  );
});

module.exports = router;
