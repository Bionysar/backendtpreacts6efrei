var express = require("express");
var router = express.Router();
var mysql = require("mysql");
var cookieParser = require("cookie-parser");
var cors = require("cors");

var connection = mysql.createConnection({
  host: "217.182.237.222",
  user: "laurent",
  password: "admin",
  database: "TopMaterielAchat",
  port: 3306
});
var result = connection.state;
router.use(cookieParser());
connection.connect(err => {
  if (!err) {
    console.log("DB connection succeded.");
    result = connection.state;
  } else
    console.log(
      "DB connection failed \n Error : " + JSON.stringify(err, undefined, 2)
    );
});

router.get("/stat", function(req, res, next) {
  connection.query(
    "SELECT billID, SUM(quantity) AS quantity_commanded FROM BillDetail GROUP BY billID",
    function(err, rows, fields) {
      if (err) {
        return console.error(err.message);
      }
      result = rows;
      if (result.length > 0) {
        var array = [];
        for (var i = 0; i < result.length; i++) {
          array.push({
            billid: result[i].billID,
            quantity: result[i].quantity_commanded
          });
        }
        res.end(JSON.stringify(array));
      }
    }
  );
});
router.get("/user", function(req, res, next) {
  connection.query(
    "SELECT u.userID, mail, userTypeID , ui.nom ,ui.prenom FROM User u INNER JOIN UserInfo ui ON u.userID = ui.userID",
    function(err, rows, fields) {
      if (err) {
        return console.error(err.message);
      }
      result = rows;
      if (result.length > 0) {
        var array = [];
        for (var i = 0; i < result.length; i++) {
          array.push({
            id: result[i].userID,
            mail: result[i].mail,
            nom: result[i].nom,
            prenom: result[i].prenom,
            type: result[i].userTypeID
          });
        }
        res.end(JSON.stringify(array));
      }
    }
  );
});
router.get("/stock", function(req, res, next) {
  connection.query(
    "SELECT productID, titre, quantity  FROM Products p INNER JOIN RefCategorie rc ON p.idCategorie = rc.idCategorie INNER JOIN RefBrand rb ON p.idBrand = rb.idBrand",
    function(err, rows, fields) {
      if (err) {
        return console.error(err.message);
      }
      result = rows;
      if (result.length > 0) {
        var array = [];
        for (var i = 0; i < result.length; i++) {
          array.push({
            id: result[i].productID,
            name: result[i].titre,
            quantity: result[i].quantity
          });
        }
        res.end(JSON.stringify(array));
      }
    }
  );
});
router.post("/changeop", function(req, res, next) {
  console.log(req.body.Data.userTypeID, req.body.Data.userID);
  if (req.body.Data.userTypeID === 1) {
    connection.query(
      " UPDATE`User` SET`userTypeID` = 2 WHERE  userID =" +
        req.body.Data.userID,
      function(err, rows, fields) {
        if (err) {
          return console.error(err.message);
        }
        result = rows;
        if (result.length > 0) {
          console.log("succes change to user");
          res.end("succes");
        }
      }
    );
  } else if (req.body.Data.userTypeID === 2) {
    connection.query(
      " UPDATE`User` SET`userTypeID` = 1 WHERE  userID =" +
        req.body.Data.userID,
      function(err, rows, fields) {
        if (err) {
          return console.error(err.message);
        }
        result = rows;
        if (result.length > 0) {
          console.log("succes change to admin");
          res.end("succes");
        }
      }
    );
  }
});
router.post("/addstock", function(req, res, next) {
  connection.query(
    "UPDATE `Products` SET `quantity` = '" +
      req.body.Data.quantity +
      "' WHERE `Products`.`productID` =" +
      req.body.Data.productID,
    function(err, rows, fields) {
      if (err) {
        return console.error(err.message);
      }
      result = rows;
      if (result.length > 0) {
        console.log("Stock added");
        res.end("Stock added");
      }
    }
  );
});
module.exports = router;
