var express = require("express");
var router = express.Router();
var mysql = require("mysql");
var connection = mysql.createConnection({
  host: "217.182.237.222",
  user: "laurent",
  password: "admin",
  database: "TopMaterielAchat",
  port: 3306
});
var result = connection.state;

connection.connect(err => {
  if (!err) {
    console.log("DB connection succeded.");
    //result = connection.state;
  } else
    console.log(
      "DB connection failed \n Error : " + JSON.stringify(err, undefined, 2)
    );
});

router.get("/", function(req, res, next) {
  connection.query("SELECT name FROM RefBrand", function(err, rows, fields) {
    if (err) {
      return console.error(err.message);
    }
    result = rows;
    if (result.length > 0) {
      var array = [];
      for (var i = 0; i < result.length; i++) {
        array.push({
          brand: result[i].name
        });
      }
      res.end(JSON.stringify(array));
    }
  });
});

module.exports = router;
