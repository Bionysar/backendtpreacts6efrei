var express = require("express");
var router = express.Router();
var mysql = require("mysql");
var cookieParser = require("cookie-parser");
var cors = require("cors");

var connection = mysql.createConnection({
  host: "217.182.237.222",
  user: "laurent",
  password: "admin",
  database: "TopMaterielAchat",
  port: 3306
});
var result = connection.state;

connection.connect(err => {
  if (!err) {
    console.log("DB connection succeded.");
    //result = connection.state;
  } else
    console.log(
      "DB connection failed \n Error : " + JSON.stringify(err, undefined, 2)
    );
});

router.post("/", function(req, res, next) {
  connection.query(
    'INSERT INTO Bill(userID, Adress, PC, City) VALUES("' +
      req.body.Data.userid +
      '","' +
      req.body.Data.adresse +
      '","' +
      req.body.Data.cp +
      '","' +
      req.body.Data.ville +
      '")',
    function(err, rows, fields) {
      if (err) {
        return console.error(err.message);
      }
      // get inserted id
      var insertId = 0;
      var billDetails = [];
      insertedId = rows.insertId;
      for (var i = 0; i < req.body.Data.products.length; i++) {
        billDetails.push([
          insertedId,
          req.body.Data.products[i].id,
          req.body.Data.products[i].quantity
        ]);
      }
      connection.query(
        "INSERT INTO BillDetail(billID, productID, quantity) VALUES ?",
        [billDetails],
        function(err, rows, fields) {
          if (err) {
            return console.error(err.message);
          }
          res.send("succes");
        }
      );
      for (var j = 0; j < req.body.Data.products.length; j++) {
        connection.query(
          'UPDATE Products SET quantity = quantity - "' +
            req.body.Data.products[j].quantity +
            '"WHERE productID = "' +
            req.body.Data.products[j].id +
            '"',
          function(err, rows, fields) {
            if (err) {
              return console.error(err.message);
            }
          }
        );
      }
    }
  );
});

module.exports = router;
